import React, { useMemo } from 'react'
import PanelHeader from '@vkontakte/vkui/dist/components/PanelHeader/PanelHeader'
import PanelHeaderButton from '@vkontakte/vkui/dist/components/PanelHeaderButton/PanelHeaderButton'
import Icon28CancelOutline from '@vkontakte/icons/dist/28/cancel_outline'
import Icon24DoneOutline from '@vkontakte/icons/dist/24/done_outline'
import useRouter from '../../../hooks/useRouter'
import { emojis, ModalId, PanelId } from '../../../App'
import { Button, FixedLayout, FormLayout, Group, HorizontalScroll, Textarea } from '@vkontakte/vkui'
import './index.scss'
import Icon20UserOutline from '@vkontakte/icons/dist/20/user_outline'
import { Icon20Dropdown, Icon20RecentOutline, Icon24SmileOutline } from '@vkontakte/icons'
import bridge from '@vkontakte/vk-bridge'

const isWebView = bridge.isWebView()

const PostCreationScreen = () => {
  const router = useRouter()
  const { emojiId } = router.data || {}

  const createPost = () => {
    router.goForward(PanelId.Posts)
  }

  const emoji = useMemo(() => {
    return emojis.find(emoji => emoji.id === emojiId)
  }, [emojiId])

  return (
    <>
      <PanelHeader
        left={<PanelHeaderButton><Icon28CancelOutline /></PanelHeaderButton>}
        right={<PanelHeaderButton style={{ marginRight: isWebView ? 100 : 0 }} onClick={createPost}><Icon24DoneOutline /></PanelHeaderButton>}
      >
        Вели
      </PanelHeader>
      <Group>
        <FormLayout style={{ marginTop: 0 }}>
          <Textarea
            placeholder="Что у вас нового?"
            className="TransparentInput"
          />

          <FixedLayout vertical="bottom" className="Buttons">
            <HorizontalScroll>
              <div style={{ display: 'flex' }}>
                <Button
                  before={(
                    !emojiId
                    ? <Icon24SmileOutline className="Button__before-icon" width={20} height={20}/>
                    : <img className="Button__emoji" src={emoji?.icon} />
                  )}
                  after={<Icon20Dropdown/>}
                  mode="outline"
                  onClick={() => router.openModal(ModalId.Mood)}
                >
                  {emoji?.name || 'Настроение'}
                </Button>


                <Button
                  before={<Icon20RecentOutline className="Button__before-icon"/>}
                  after={<Icon20Dropdown/>}
                  mode="outline"
                >
                  Сейчас
                </Button>

                <Button
                  before={<Icon20UserOutline className="Button__before-icon"/>}
                  after={<Icon20Dropdown/>}
                  mode="outline"
                >
                  Тематика
                </Button>
              </div>
            </HorizontalScroll>
          </FixedLayout>
        </FormLayout>
      </Group>
    </>
  )
}

export default PostCreationScreen

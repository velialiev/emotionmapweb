import React from 'react'
import Header from '../../../components/Header'
import useRouter from '../../../hooks/useRouter'
import { Emoji } from '../../../App'
import { Avatar, Card, CardGrid, Div, Group, Separator, SimpleCell, Tabbar, TabbarItem, Text } from '@vkontakte/vkui'
import {
  Icon20ViewOutline,
  Icon28ClipOutline,
  Icon28CommentOutline,
  Icon28LikeOutline,
  Icon28MessageOutline,
  Icon28MoreVertical,
  Icon28NewsfeedOutline,
  Icon28ServicesOutline,
  Icon28ShareOutline,
  Icon28UserCircleOutline
} from '@vkontakte/icons'
import AliAvatar from '../../../assets/images/Ali.jpg'
import VeliAvatar from '../../../assets/images/Veli.jpg'
import DimaAvatar from '../../../assets/images/Dima.jpg'
import IvanAvatar from '../../../assets/images/Ivan.jpg'

const posts = [
  {
    id: 1,
    time: 'только что',
    avatar: VeliAvatar,
    authorName: 'Вели Алиев',
    text: 'С другой стороны постоянный количественный рост и сфера нашей активности обеспечивает широкому ' +
      'кругу (специалистов) участие в формировании системы обучения кадров, соответствует насущным потребностям. ' +
      'Значимость этих проблем настолько очевидна, что новая модель организационной деятельности способствует ' +
      'подготовки и реализации модели развития. Значимость этих проблем настолько очевидна, что рамки и место ' +
      'обучения кадров позволяет оценить значение позиций, занимаемых участниками в отношении поставленных задач. ' +
      'Значимость этих проблем настолько очевидна, что дальнейшее развитие различных форм деятельности играет ' +
      'важную роль в формировании системы обучения кадров, соответствует насущным потребностям. ' +
      'Не следует, однако забывать, что постоянное информационно-пропагандистское обеспечение нашей деятельности ' +
      'требуют определения и уточнения существенных финансовых и административных условий. ',
  },
  {
    id: 2,
    time: 'минуту назад',
    avatar: AliAvatar,
    authorName: 'Али Алиев',
    text: 'Задача организации, в особенности же дальнейшее развитие различных форм деятельности обеспечивает ' +
      'широкому кругу (специалистов) участие в формировании форм развития. Равным образом укрепление и ' +
      'развитие структуры способствует подготовки и реализации существенных финансовых и административных условий. ',
  },
  {
    id: 3,
    time: 'час назад',
    avatar: IvanAvatar,
    authorName: 'Иван Касаткин',
    text: 'С другой стороны реализация намеченных плановых заданий способствует подготовки и реализации позиций, ' +
      'занимаемых участниками в отношении поставленных задач. Повседневная практика показывает, что рамки ' +
      'и место обучения кадров требуют от нас анализа направлений прогрессивного развития. Разнообразный и ' +
      'богатый опыт постоянное информационно-пропагандистское обеспечение нашей деятельности представляет ' +
      'собой интересный эксперимент проверки направлений прогрессивного развития. Товарищи! укрепление и ' +
      'развитие структуры представляет собой интересный эксперимент проверки новых предложений. ',
  },
  {
    id: 4,
    time: '2 часа назад',
    avatar: DimaAvatar,
    authorName: 'Дмитрий Московский',
    text: 'Товарищи! дальнейшее развитие различных форм деятельности в значительной степени обуславливает создание ' +
      'соответствующий условий активизации. Идейные соображения высшего порядка, а также новая модель ' +
      'организационной деятельности способствует подготовки и реализации модели развития. С другой стороны ' +
      'начало повседневной работы по формированию позиции способствует подготовки и реализации существенных ' +
      'финансовых и административных условий. ',
  },
]

const CategoryScreen = () => {
  const router = useRouter()
  const { emoji }: RouterData = router.data

  return (
    <>
      <Header>{emoji.name || 'Категория'}</Header>

      <Group style={{ paddingBottom: '60px' }}>
        <CardGrid style={{ paddingLeft: 0, paddingRight: 0, borderRadius: 0, }}>
          {posts.map(post => (
            <Card
              key={post.id}
              size="l"
              mode="shadow"
              className="Post"
            >
              <SimpleCell
                before={<Avatar size={40} src={post.avatar}/>}
                after={<Icon28MoreVertical/>}
                description={post.time}
              >
                {post.authorName}
              </SimpleCell>
              <Div style={{ paddingBottom: 0 }}>
                <Text weight="regular">
                  {post.text}
                </Text>
                <Separator wide style={{ margin: '12px 0 0 0' }}/>

                <div className="PostFooter">
                  <div className="PostFooter__actions">
                    <Icon28LikeOutline fill="#99A2AD"/>
                    <Icon28CommentOutline fill="#99A2AD"/>
                    <Icon28ShareOutline fill="#99A2AD"/>
                  </div>
                  <Text className="PostFooter__views" weight="regular">
                    <Icon20ViewOutline fill="#99A2AD"/>
                    26к
                  </Text>
                </div>
              </Div>
            </Card>
          ))}
        </CardGrid>
      </Group>

      <Tabbar>
        <TabbarItem
          data-story="feed"
          text="Новости"
          selected={true}
        ><Icon28NewsfeedOutline/></TabbarItem>
        <TabbarItem
          data-story="services"
          text="Сервисы"
        ><Icon28ServicesOutline/></TabbarItem>
        <TabbarItem
          data-story="messages"
          label="12"
          text="Сообщения"
        ><Icon28MessageOutline/></TabbarItem>
        <TabbarItem
          data-story="clips"
          text="Клипы"
        ><Icon28ClipOutline/></TabbarItem>
        <TabbarItem
          data-story="profile"
          text="Профиль"
        ><Icon28UserCircleOutline/></TabbarItem>
      </Tabbar>
    </>
  )
}

interface RouterData {
  emoji: Emoji,
}

export default CategoryScreen

import React, { useMemo, useState } from 'react'
import ReactMapboxGl, { Cluster, Marker } from 'react-mapbox-gl'
import { Avatar, Caption, Counter, Div, FixedLayout, FormLayout, HorizontalScroll, Input, Text } from '@vkontakte/vkui'
import './index.scss'
import { Emoji, emojis, PanelId } from '../../../App'
import useRouter from '../../../hooks/useRouter'
import { Icon16Done } from '@vkontakte/icons'

const Map = ReactMapboxGl({
  accessToken: 'pk.eyJ1IjoiaGFyb2RpZXIxMCIsImEiOiJja2Y4ZnFuZnEwM2p6MzJsZGdqOHFqbjlwIn0.BquFfL4AvZzVkLEJu5Gwsg',
})

const EmotionMapScreen = () => {
  const router = useRouter()
  const [search, setSearch] = useState('')
  const [activeEmojiFilters, setActiveEmojiFilters] = useState<Emoji['id'][]>([])

  const searchedEmojiFilters = useMemo(() => {
    return emojis.filter(emoji => emoji.name.toLowerCase().includes(search.toLowerCase()))
  }, [search])

  const filteredEmojis = useMemo(() => {
    if (activeEmojiFilters.length === 0) {
      return emojis
    }

    return emojis.filter(emoji => activeEmojiFilters.includes(emoji.id))
  }, [activeEmojiFilters])

  const clusterMarker = (coordinates: GeoJSON.Position, emoji: Emoji, count: number) => (
    <Marker
      onClick={() => router.goForward(PanelId.Category, { emoji })}
      coordinates={coordinates}
      anchor="bottom">
      <div className="EmojiWrap">
        <img width={32} height={32} src={emoji.icon}/>
        <div className="EmojiWrap__badge">
          <Counter size="s" mode="primary">{count}</Counter>
        </div>
      </div>
    </Marker>
  )

  const isFilterActive = (emoji: Emoji) => {
    return activeEmojiFilters.includes(emoji.id)
  }

  const selectFilter = (emoji: Emoji) => {
    if (isFilterActive(emoji)) {
      setActiveEmojiFilters(activeEmojiFilters.filter(emojiId => emojiId !== emoji.id ))
      return
    }

    setActiveEmojiFilters([
      ...activeEmojiFilters,
      emoji.id,
    ])
  }

  return (
    <>
      <Map
        style="mapbox://styles/mapbox/streets-v9"
        containerStyle={{
          height: '100vh',
          width: '100vw'
        }}
        center={[34.1107900, 44.9571900]}
      >
        {/*<Layer*/}
        {/*  type="symbol"*/}
        {/*  layout={{ 'shape-source': emojis[0].icon }}*/}
        {/*>*/}
        {/*  */}
        {/*</Layer>*/}

        {filteredEmojis.map(emoji => (
          <Cluster ClusterMarkerFactory={(coordinates: GeoJSON.Position, count: number) => (
            clusterMarker(coordinates, emoji, count)
          )}>
            {emoji.coordinates.map(coordinate => (
              <Marker
                onClick={() => router.goForward(PanelId.Category, { emoji, })}
                coordinates={coordinate}
                anchor="bottom">
                <div className="EmojiWrap">
                  <img width={32} height={32} src={emoji.icon}/>
                </div>
              </Marker>
            ))}
          </Cluster>
        ))}
      </Map>

      <FixedLayout vertical="bottom" className="MapFooter" style={{ minHeight: 204 }}>
        <FormLayout>
          <Input
            value={search}
            onChange={e => setSearch(e.target.value)}
            type="text"
            placeholder="Поиск по темам и настроению"
          />

          <HorizontalScroll>
            <Div style={{ display: 'flex', paddingBottom: 0, paddingTop: 0 }}>
              {searchedEmojiFilters?.length
                ? searchedEmojiFilters.map(emoji => (
                  <div
                    onClick={() => selectFilter(emoji)}
                    className={`Emojis__wrap ${isFilterActive(emoji) ? 'Emojis__wrap--active' : ''}`}
                    style={{ marginRight: 16 }}
                  >
                    <Avatar className="Emojis__emoji" size={72} src={emoji.icon}>
                      <div
                        className="Emojis__badge"
                        style={{
                          opacity: isFilterActive(emoji) ? 1 : 0
                        }}
                      >
                        <Icon16Done fill="#fff" />
                      </div>
                    </Avatar>
                    <Caption level="1" weight="regular" style={{ marginBottom: 16 }}>
                      {emoji.name}
                    </Caption>
                  </div>
                ))
                : <Text weight="regular">Нет результатов :(</Text>}
            </Div>
          </HorizontalScroll>
        </FormLayout>
      </FixedLayout>
    </>
  )
}

export default EmotionMapScreen

import React, { useState } from 'react'
import {
  Avatar,
  Button,
  Card,
  CardGrid,
  Cell,
  Div,
  FixedLayout,
  Group,
  PanelHeader,
  PanelHeaderButton,
  SimpleCell,
  Tabbar,
  TabbarItem,
  Tabs,
  TabsItem,
  Text,
} from '@vkontakte/vkui'
import {
  Icon16Dropdown,
  Icon24Globe,
  Icon28CameraOutline,
  Icon28ClipOutline,
  Icon28LikeOutline,
  Icon28MessageOutline,
  Icon28MoreHorizontal,
  Icon28NewsfeedOutline,
  Icon28Notifications,
  Icon28ServicesOutline,
  Icon28ShareOutline,
  Icon28UserCircleOutline
} from '@vkontakte/icons'
import AliAvatar from '../../../assets/images/Ali.jpg'
import VeliAvatar from '../../../assets/images/Veli.jpg'
import DimaAvatar from '../../../assets/images/Dima.jpg'
import IvanAvatar from '../../../assets/images/Ivan.jpg'
import bg1 from '../../../assets/images/bg1.png'
import bg2 from '../../../assets/images/bg2.png'
import './index.scss'
import useRouter from '../../../hooks/useRouter'
import { PanelId } from '../../../App'

enum Tab {
  News = 'News',
  Trend = 'Trend',
}

const posts = [
  {
    id: 1,
    name: 'Али Алиев',
    commentsCount: 16,
    avatar: AliAvatar,
    postImage: bg1,
  },
  {
    id: 2,
    name: 'Вели Алиев',
    commentsCount: 9,
    avatar: VeliAvatar,
    postImage: bg2,
  },
  {
    id: 3,
    name: 'Дмитрий Московский',
    commentsCount: 25,
    avatar: DimaAvatar,
    postImage: bg1,
  },
  {
    id: 4,
    name: 'Иван Касаткин',
    commentsCount: 14,
    avatar: IvanAvatar,
    postImage: bg2,
  },
]

const PostsScreen = () => {
  const router = useRouter()

  const [activeTab, setActiveTab] = useState<Tab>(Tab.Trend)
  return (
    <>
      <PanelHeader
        left={<PanelHeaderButton><Icon28CameraOutline/></PanelHeaderButton>}
        right={<PanelHeaderButton><Icon28Notifications/></PanelHeaderButton>}
        separator={false}
      >
        <Tabs>
          <TabsItem
            onClick={() => {
              setActiveTab(Tab.News)
            }}
            selected={activeTab === Tab.News}
            after={<Icon16Dropdown fill="var(--accent)"/>}
          >
            Новости
          </TabsItem>
          <TabsItem
            onClick={() => {
              setActiveTab(Tab.Trend)
            }}
            selected={activeTab === Tab.Trend}
          >
            Интересное
          </TabsItem>
        </Tabs>
      </PanelHeader>

      <Group style={{ paddingBottom: '60px' }}>
        <CardGrid>
          {posts.map(post => (
            <Card
              key={post.id}
              size="l"
              mode="shadow"
              className="Post"
            >
              <SimpleCell
                style={{ paddingTop: 6, paddingBottom: 6 }}
                before={<Avatar size={40} src={post.avatar}/>}
              >
                {post.name}
              </SimpleCell>
              <img width="100%" src={post.postImage} alt="" style={{ objectFit: 'cover' }}/>
              <Div style={{ paddingBottom: 0 }}>
                <Text weight="regular" style={{ color: 'var(--text_secondary)' }}>
                  {post.commentsCount} комментариев
                </Text>
                <Cell className="Icons"
                      before={<Icon28LikeOutline fill="#99A2AD"/>}
                      indicator={<Icon28MoreHorizontal fill="#99A2AD"/>}
                >
                  <Icon28ShareOutline fill="#99A2AD"/>
                </Cell>
              </Div>
            </Card>
          ))}
        </CardGrid>

        <FixedLayout vertical="bottom">
          <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
            <Button
              onClick={() => router.goForward(PanelId.EmotionMap)}
              className="MapNavigationButton">
              <Icon24Globe/>
            </Button>
          </div>
        </FixedLayout>
      </Group>

      <Tabbar>
        <TabbarItem
          data-story="feed"
          text="Новости"
          selected={true}
        ><Icon28NewsfeedOutline/></TabbarItem>
        <TabbarItem
          data-story="services"
          text="Сервисы"
        ><Icon28ServicesOutline/></TabbarItem>
        <TabbarItem
          data-story="messages"
          label="12"
          text="Сообщения"
        ><Icon28MessageOutline/></TabbarItem>
        <TabbarItem
          data-story="clips"
          text="Клипы"
        ><Icon28ClipOutline/></TabbarItem>
        <TabbarItem
          data-story="profile"
          text="Профиль"
        ><Icon28UserCircleOutline/></TabbarItem>
      </Tabbar>
    </>
  )
}

export default PostsScreen

import React, { FC } from 'react'
import { PanelHeader, PanelHeaderBack } from '@vkontakte/vkui'
import useRouter from '../../hooks/useRouter'

const Header: FC<Props> = ({ hideBackButton, onBackButtonClick, children }) => {
  const router = useRouter()

  return (
    <PanelHeader
      left={
        !hideBackButton && router.history.length > 1 ? (
          <PanelHeaderBack
            onClick={() => {
              router.goBack()

              if (onBackButtonClick) {
                onBackButtonClick()
              }
            }}
          />
        ) : null
      }
    >
      {children}
    </PanelHeader>
  )
}

interface Props {
  hideBackButton?: boolean
  onBackButtonClick?: () => void
}

export default Header

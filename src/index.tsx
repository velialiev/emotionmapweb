import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import bridge from '@vkontakte/vk-bridge'

bridge.send('VKWebAppInit', {})

const render = () => {
  ReactDOM.render(
    <React.StrictMode>
      <App/>
    </React.StrictMode>,
    document.getElementById('root'),
  )
}

render()

if (process.env.NODE_ENV === 'development' && module.hot) {
  module.hot.accept('./App', render)
}

const callTimes = (fn: Function, times: number) => {
  return Array(times).fill(null)
    .map(() => fn())
}

export default callTimes

import React, { useState } from 'react'
import '@vkontakte/vkui/dist/vkui.css'
import bridge from '@vkontakte/vk-bridge'
import {
  Avatar,
  Caption,
  Div,
  ModalPage,
  ModalPageHeader,
  ModalRoot,
  OS,
  Panel,
  PanelHeaderButton,
  Root,
  usePlatform,
  View
} from '@vkontakte/vkui'
import PostCreationScreen from '../features/EmotionMap/PostCreationScreen'
import PostsScreen from '../features/EmotionMap/PostsScreen'
import EmotionMapScreen from '../features/EmotionMap/EmotionMapScreen'
import CategoryScreen from '../features/EmotionMap/CategoryScreen'
import Icon24Cancel from '@vkontakte/icons/dist/24/cancel'
import Icon24Done from '@vkontakte/icons/dist/24/done'
import embrace from '../assets/emojis/embrace.png'
import fool from '../assets/emojis/fool.png'
import happy from '../assets/emojis/happy.png'
import joy from '../assets/emojis/joy.png'
import love from '../assets/emojis/love.png'
import shyness from '../assets/emojis/shyness.png'
import smile from '../assets/emojis/smile.png'
import './index.scss'
import getRandomArbitrary from '../utils/getRandomArbitrary'
import callTimes from '../utils/callTimes'
import { Icon16Done } from '@vkontakte/icons'

const leftBottomCornerCoordinates = [34.06, 44.9064]
const rightTopCornerCoordinates = [34.16158, 45.00798]

const generateRandomCoordinates = () => {
  return [
    getRandomArbitrary(leftBottomCornerCoordinates[0], rightTopCornerCoordinates[0]),
    getRandomArbitrary(leftBottomCornerCoordinates[1], rightTopCornerCoordinates[1]),
  ]
}

export const emojis: Emoji[] = [
  {
    id: 1, name: 'Любовь', icon: love, coordinates: [
      ...callTimes(generateRandomCoordinates, 3),
    ],
  },
  {
    id: 2, name: 'Дурачимся', icon: fool, coordinates: [
    ...callTimes(generateRandomCoordinates, 3),
    ],
  },
  {
    id: 3, name: 'Веселимся', icon: happy, coordinates: [
      ...callTimes(generateRandomCoordinates, 3),
    ],
  },
  {
    id: 4, name: 'Радость', icon: joy, coordinates: [
      ...callTimes(generateRandomCoordinates, 3),
    ],
  },
  {
    id: 5, name: 'Обнимашки', icon: embrace, coordinates: [
      ...callTimes(generateRandomCoordinates, 3),
    ],
  },
  {
    id: 6, name: 'Неловкость', icon: shyness, coordinates: [
      ...callTimes(generateRandomCoordinates, 3),
    ],
  },
  {
    id: 7, name: 'Счастье', icon: smile, coordinates: [
      ...callTimes(generateRandomCoordinates, 3),
    ],
  },
]

export type Emoji = {
  id: number
  name: string
  icon: string
  coordinates: GeoJSON.Position[]
}

export enum ViewId {
  EmotionMap = 'EmotionMap',
}

export enum PanelId {
  PostCreation = 'PostCreation',
  Posts = 'Posts',
  EmotionMap = 'EmotionMap',
  Category = 'Category',
  Modal = 'Modal',
}

export enum ModalId {
  Mood = 'Mood',
}

export const RouterContext = React.createContext<RouterContextValue>({
  currentViewId: ViewId.EmotionMap,
  currentPanelId: PanelId.PostCreation,
  goBack: () => {
  },
  goForward: () => {
  },
  history: [PanelId.PostCreation],
  openModal: () => {

  },
})

type RouterContextValue = {
  currentViewId: ViewId
  currentPanelId: PanelId
  goBack: () => void
  goForward: (panelId: PanelId, data?: any) => void
  history: PanelId[]
  data?: any
  setData?: (data: any) => void
  openModal: (modalId: ModalId) => void
}

const App = () => {
  const [currentViewId] = useState<ViewId>(ViewId.EmotionMap)
  const [currentPanelId, setCurrentPanelId] = useState<PanelId>(PanelId.PostCreation)
  const [currentModalId, setCurrentModalId] = useState<ModalId | null>(null)
  const [history, setHistory] = useState<PanelId[]>([PanelId.PostCreation])
  const [routerData, setRouterData] = useState()
  const platform = usePlatform()

  const goBack = () => {
    const updatedHistory = [...history]
    updatedHistory.pop()
    const panelId = updatedHistory[updatedHistory.length - 1]

    if (panelId === PanelId.PostCreation) {
      bridge.send('VKWebAppDisableSwipeBack')
    }

    setHistory(updatedHistory)
    setCurrentPanelId(panelId)
  }

  const goForward = (panelId: PanelId, data?: any) => {
    const updatedHistory = [...history]
    updatedHistory.push(panelId)

    if (currentPanelId === PanelId.PostCreation) {
      bridge.send('VKWebAppEnableSwipeBack')
    }

    setHistory(updatedHistory)
    setCurrentPanelId(panelId)
    setRouterData(data)
  }

  const openModal = (modalId: ModalId) => {
    setCurrentModalId(modalId)
  }

  const closeModal = () => {
    setCurrentModalId(null)
  }

  const modal = (
    <ModalRoot
      activeModal={currentModalId}
      onClose={closeModal}
    >
      <ModalPage
        id={ModalId.Mood}
        onClose={closeModal}
        header={
          <ModalPageHeader
            left={platform === OS.ANDROID &&
            <PanelHeaderButton onClick={closeModal}><Icon24Cancel/></PanelHeaderButton>}
            right={(
              <PanelHeaderButton
                onClick={closeModal}
              >
                {platform === OS.ANDROID ? 'Готово' : <Icon24Done/>}
              </PanelHeaderButton>
            )}
          >
            Настроение
          </ModalPageHeader>
        }
      >
        <Div className="Emojis">
          {emojis.map(emoji => (
            <div className={`Emojis__wrap ${emoji.id === routerData?.emojiId ? 'Emojis__wrap--active' : ''}`}
                 onClick={() => setRouterData({ emojiId: emoji.id })}
            >
              <Avatar className="Emojis__emoji" size={72} src={emoji.icon}>
                <div
                  className="Emojis__badge"
                  style={{
                    opacity: emoji.id === routerData?.emojiId ? 1 : 0
                  }}
                >
                  <Icon16Done fill="#fff" />
                </div>
              </Avatar>
              <Caption level="1" weight="regular" style={{ marginBottom: 16 }}>
                {emoji.name}
              </Caption>
            </div>
          ))}
        </Div>
      </ModalPage>
    </ModalRoot>
  )


  return (
    <RouterContext.Provider
      value={{
        currentViewId,
        currentPanelId,
        data: routerData,
        setData: setRouterData,
        goBack,
        goForward,
        history,
        openModal,
      }}
    >
      <Root activeView={currentViewId}>
        <View id={ViewId.EmotionMap} activePanel={currentPanelId} modal={modal}>
          <Panel id={PanelId.PostCreation}>
            <PostCreationScreen/>
          </Panel>

          <Panel id={PanelId.Posts}>
            <PostsScreen/>
          </Panel>

          <Panel id={PanelId.EmotionMap}>
            <EmotionMapScreen/>
          </Panel>

          <Panel id={PanelId.Category}>
            <CategoryScreen/>
          </Panel>

          <Panel id={PanelId.Modal}>{routerData?.renderContent?.()}</Panel>
        </View>
      </Root>
    </RouterContext.Provider>
  )
}

export default App
